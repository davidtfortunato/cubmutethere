package isec.cub.mutethere.base.data;

/**
 * Created by David Fortunato on 19/07/2017
 * All rights reserved GoodBarber
 */

public class PointSoundAlertEntity extends PointAlertEntity
{

    private SoundAlertMode soundAlertMode;

    public PointSoundAlertEntity()
    {
        super(PointType.SOUND);
    }

    public SoundAlertMode getSoundAlertMode()
    {
        return soundAlertMode;
    }

    public void setSoundAlertMode(SoundAlertMode soundAlertMode)
    {
        this.soundAlertMode = soundAlertMode;
    }

    public enum SoundAlertMode
    {
        ACTIVATE_SOUND, MUTE_SOUND;
    }

}
