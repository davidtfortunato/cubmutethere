package isec.cub.mutethere.base.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import isec.cub.mutethere.MainActivity;

/**
 * Created by David Fortunato on 27/06/2017
 * All rights reserved GoodBarber
 */

public abstract class BaseFragment extends Fragment implements FragmentManager.OnBackStackChangedListener
{
    private ViewGroup mFragmentContainer;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // On Backstack changed
        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        if (mFragmentContainer == null)
        {
            mFragmentContainer = (ViewGroup) inflater.inflate(getLayoutId(), container, false);

            initUI(mFragmentContainer);

        }
        return mFragmentContainer;
    }

    @Override
    public void onBackStackChanged()
    {
        onResume();
    }

    protected abstract int getLayoutId();

    protected abstract void initUI(ViewGroup layoutContent);

    public void navigateToFragment(Fragment fragment)
    {
        if (getActivity() instanceof MainActivity)
        {
            ((MainActivity) getActivity()).fragmentNavigation(fragment);
        }
    }
}
