package isec.cub.mutethere.base.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;
import java.util.List;
import isec.cub.mutethere.base.controllers.CUBLocationManager;
import isec.cub.mutethere.base.data.LocationEntity;
import isec.cub.mutethere.base.data.PointAlertEntity;
import isec.cub.mutethere.base.data.PointMemoAlertEntity;
import isec.cub.mutethere.base.data.PointSoundAlertEntity;
import isec.cub.mutethere.base.data.managers.PointsManager;

/**
 * Created by David Fortunato on 23/07/2017
 * All rights reserved GoodBarber
 */

public class AlertLocationService extends Service implements CUBLocationManager.OnReportLocation
{
    private static boolean isRunning = false;

    // Location Manager
    private CUBLocationManager locationManager;

    @Override
    public void onCreate()
    {
        super.onCreate();

        // Init Location Manager
        locationManager = new CUBLocationManager(getBaseContext(), this);
        locationManager.initLocation();

        isRunning = true;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public void onReportLocation(LocationEntity locationEntity)
    {
        List<PointAlertEntity> listAlerts = PointsManager.getInstance().getListAlerts();

        for (PointAlertEntity alertEntity : listAlerts)
        {
            if (alertEntity.runCurrentLocation(locationEntity.getLocation()))
            {
                Toast.makeText(getBaseContext(), "Alert: " + alertEntity.getName(), Toast.LENGTH_SHORT).show();
                switch (alertEntity.getPointType())
                {
                    case MEMO:
                        PointsManager.getInstance().executeMemoAlert((PointMemoAlertEntity) alertEntity);
                        break;
                    case SOUND:
                        PointsManager.getInstance().executeSoundAlert((PointSoundAlertEntity) alertEntity);
                        break;
                }
            }
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        isRunning = false;
    }

    public static void startService(Context context)
    {
        if (!isRunning)
        {
            Intent serviceIntent = new Intent(context, AlertLocationService.class);
            context.startService(serviceIntent);
        }
    }
}
