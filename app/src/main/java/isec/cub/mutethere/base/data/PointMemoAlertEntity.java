package isec.cub.mutethere.base.data;

/**
 * Created by David Fortunato on 19/07/2017
 * All rights reserved GoodBarber
 */

public class PointMemoAlertEntity extends PointAlertEntity
{
    private MemoRepeatMode memoRepeatMode;

    public PointMemoAlertEntity()
    {
        super(PointType.MEMO);
    }

    public MemoRepeatMode getMemoRepeatMode()
    {
        return memoRepeatMode;
    }

    public void setMemoRepeatMode(MemoRepeatMode memoRepeatMode)
    {
        this.memoRepeatMode = memoRepeatMode;
    }

    public enum MemoRepeatMode
    {
        ONE_SHOT, EVERY_TIME_INSIDE;
    }
}
