package isec.cub.mutethere.base.controllers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import isec.cub.mutethere.base.data.LocationEntity;
import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by David Fortunato on 15/03/2017
 * All rights reserved GoodBarber
 */

public class CUBLocationManager implements LocationListener
{
    private static final String TAG = CUBLocationManager.class.getSimpleName();

    // TAGS
    private static final int TAG_CODE_PERMISSION_LOCATION = 100;
    private static final int TIME_TO_NEW_LOCATION = 5000;
    private static final int DISTANCE_TO_NEW_LOCATION = 10;

    private List<String> listProviders;

    // Data
    private Context          mActivity;
    private OnReportLocation onReportLocationListener;
    private LocationEntity   lastLocationEntity;

    // Dynamic data
    private int indexCurrentProvider;

    // Manager
    private LocationManager mLocationManager;

    public CUBLocationManager(Context activity, OnReportLocation onReportLocationListener)
    {
        this.mActivity = activity;
        this.onReportLocationListener = onReportLocationListener;


        this.listProviders = new ArrayList<>();
        this.listProviders.add(LocationManager.GPS_PROVIDER);
        this.listProviders.add(LocationManager.NETWORK_PROVIDER);

        indexCurrentProvider = 0;
    }


    public void initLocation()
    {
        // Request permission
        if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            mLocationManager = (LocationManager) mActivity.getSystemService(LOCATION_SERVICE);

            boolean foundLastLocation = false;
            for (String str : listProviders)
            {
                Location lastLocation = mLocationManager.getLastKnownLocation(str);
                if (!foundLastLocation && lastLocation != null)
                {
                    reportLocation(lastLocation, false);
                    foundLastLocation = true;
                }

            }
            String strBetterProvider = getBestProvider();
            mLocationManager.requestLocationUpdates(strBetterProvider, 500, DISTANCE_TO_NEW_LOCATION, this);
            return;
        }
        else
        {
            if (mActivity instanceof Activity)
            {
                ActivityCompat.requestPermissions((Activity) mActivity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                                  TAG_CODE_PERMISSION_LOCATION);
            }

        }
    }

    private String getBestProvider()
    {
        Criteria criteria = new Criteria();
        criteria.setAltitudeRequired(true);
        return mLocationManager.getBestProvider(criteria, true);
    }

    public void requestSingleUpdate()
    {
        // Request permission
        if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            mLocationManager = (LocationManager) mActivity.getSystemService(LOCATION_SERVICE);

            mLocationManager.requestSingleUpdate(getBestProvider(), this, Looper.myLooper());
            return;
        }
        else
        {
            if (mActivity instanceof Activity)
            {
                ActivityCompat.requestPermissions((Activity) mActivity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                                  TAG_CODE_PERMISSION_LOCATION);
            }
        }
    }


    private void reportLocation(Location location, boolean shouldNotify)
    {
        if (location != null)
        {
            if (lastLocationEntity == null)
            {
                lastLocationEntity = new LocationEntity();
            }
            else if (!isBetterLocation(location, lastLocationEntity.getLocation()) || !location.hasAltitude())
            {
                // Is not good than the other
                return;
            }
            lastLocationEntity.setLat(String.valueOf(location.getLatitude()));
            lastLocationEntity.setLng(String.valueOf(location.getLongitude()));
            lastLocationEntity.setAlt(String.valueOf(location.getAltitude()));
            lastLocationEntity.setLocation(location);

            if (onReportLocationListener != null && shouldNotify)
            {
                onReportLocationListener.onReportLocation(lastLocationEntity);
            }
        }
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        initLocation();
    }


    @Override
    public void onLocationChanged(Location location)
    {
        Log.d(TAG, "On Location Changed: " + location.getLatitude() + ", " + location.getLongitude());

        reportLocation(location, true);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle)
    {
        Log.d(TAG, "onStatusChanged: " + s);
    }

    @Override
    public void onProviderEnabled(String s)
    {
        initLocation();
    }

    @Override
    public void onProviderDisabled(String s)
    {
        initLocation();
    }

    public LocationEntity getLastLocationEntity()
    {
        return lastLocationEntity;
    }


    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TIME_TO_NEW_LOCATION;
        boolean isSignificantlyOlder = timeDelta < -TIME_TO_NEW_LOCATION;
        boolean isNewer = timeDelta > 0;

        // If it's been more than 5 seconds since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than 5 seconds older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                                                    currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }


    public static interface OnReportLocation
    {
        void onReportLocation(LocationEntity locationEntity);
    }

}
