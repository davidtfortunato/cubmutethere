package isec.cub.mutethere.base.data;

import android.graphics.Color;
import android.location.Location;
import android.widget.Toast;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import isec.cub.mutethere.CUBApplication;
import isec.cub.mutethere.Utils;
import isec.cub.mutethere.base.data.managers.PointsManager;

/**
 * Created by David Fortunato on 19/07/2017
 * All rights reserved GoodBarber
 */

public class PointAlertEntity implements Serializable
{

    // Default Values
    public static final String SOUND_COLOR = "#66FF0000";
    public static final String MEMO_COLOR  = "#6600FFFF";

    private String        id;
    private String        name;
    private Location      centerLocation;
    private double        radius;
    private PointType     pointType;
    private boolean       inside;
    private String        dailyHourStart; // The daily hour that should be considered this alert
    private String        dailyHourEnd; //  The daily hour that should be considered this alert
    private CircleOptions circleOptions;
    private long          timeStampLastTimeAlerted;

    public PointAlertEntity(PointType pointType)
    {
        this.pointType = pointType;
        this.dailyHourStart = this.dailyHourEnd = "-1";
        this.id = String.valueOf(Calendar.getInstance().getTimeInMillis());
        this.timeStampLastTimeAlerted = -1;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Location getCenterLocation()
    {
        return centerLocation;
    }

    public void setCenterLocation(Location centerLocation)
    {
        this.centerLocation = centerLocation;
    }

    public double getRadius()
    {
        return radius;
    }

    public void setRadius(double radius)
    {
        this.radius = radius;
    }

    public PointType getPointType()
    {
        return pointType;
    }

    public void setPointType(PointType pointType)
    {
        this.pointType = pointType;
    }

    public boolean isInside()
    {
        return inside;
    }

    public void setInside(boolean inside)
    {
        this.inside = inside;
    }

    public long getTimeStampLastTimeAlerted()
    {
        return timeStampLastTimeAlerted;
    }

    public void setTimeStampLastTimeAlerted(long timeStampLastTimeAlerted)
    {
        this.timeStampLastTimeAlerted = timeStampLastTimeAlerted;
        PointsManager.getInstance().saveData();
    }

    public String getDailyHourStart()
    {
        return dailyHourStart;
    }

    public void setDailyHourStart(String dailyHourStart)
    {
        this.dailyHourStart = dailyHourStart;
    }

    public String getDailyHourEnd()
    {
        return dailyHourEnd;
    }

    public void setDailyHourEnd(String dailyHourEnd)
    {
        this.dailyHourEnd = dailyHourEnd;
    }

    public int getColor()
    {
        switch (pointType)
        {
            case SOUND:
                return Color.parseColor(SOUND_COLOR);
            case MEMO:
                return Color.parseColor(MEMO_COLOR);
        }
        return Color.BLUE;
    }

    public LatLng getLatLng()
    {
        return new LatLng(centerLocation.getLatitude(), centerLocation.getLongitude());
    }

    public CircleOptions getCircleOptions()
    {
        if (circleOptions == null)
        {
            circleOptions = new CircleOptions().radius(getRadius()).fillColor(getColor()).center(getLatLng()).strokeWidth(0).clickable(true);
        }

        return circleOptions;
    }

    public boolean isInsideTime()
    {
        if (getDailyHourStart().equals(getDailyHourEnd()))
        {
            return true;
        }
        else
        {
            Date date = Calendar.getInstance().getTime();
            int startTime[] = Utils.parseTimeStringToTimeArray(getDailyHourStart());
            int endTime[] = Utils.parseTimeStringToTimeArray(getDailyHourEnd());
            int currentTime[] = new int[2];
            currentTime[0] = date.getHours();
            currentTime[1] = date.getMinutes();

            return Utils.isArrTimeAfter(startTime, currentTime) && Utils.isArrTimeBefore(endTime, currentTime);
        }
    }

    public boolean runCurrentLocation(Location currentLocation)
    {
        if (isInsideTime())
        {
            boolean isInside = isLocationInsidePointRadius(currentLocation);

            if (isInside == this.isInside())
            {
                // Check if accomplish the request
                if (timeStampLastTimeAlerted == -1)
                {
                    // Should alert and update timestamp
                    setTimeStampLastTimeAlerted(Calendar.getInstance().getTimeInMillis());
                    return true;
                }
            }
            else
            {
                setTimeStampLastTimeAlerted(-1);
            }
        }
        return false;
    }

    public boolean isLocationInsidePointRadius(Location locationToValidate)
    {
        float[] distance = new float[2];

        Location.distanceBetween(locationToValidate.getLatitude(), locationToValidate.getLongitude(), getCircleOptions().getCenter().latitude,
                                 getCircleOptions().getCenter().longitude, distance);

        if (distance[0] > circleOptions.getRadius())
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void cleanDummyData()
    {
        circleOptions = null;
    }

    public boolean isValidData()
    {
        // Validate date
        int startTime[] = Utils.parseTimeStringToTimeArray(getDailyHourStart());
        int endTime[] = Utils.parseTimeStringToTimeArray(getDailyHourEnd());
        if (!Utils.isArrTimeAfter(startTime, endTime))
        {
            Toast.makeText(CUBApplication.getContext(), "A data final tem de ser superior à data inicial!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (name == null || name.isEmpty())
        {
            Toast.makeText(CUBApplication.getContext(), "Deve inserir um nome neste alerta", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public enum PointType
    {
        SOUND, MEMO;
    }
}
