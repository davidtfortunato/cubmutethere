package isec.cub.mutethere.base.data;

import android.location.Location;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by David Fortunato on 01/05/2017
 * All rights reserved GoodBarber
 */

public class LocationEntity
{
    private String lat;
    private String lng;
    private String alt;
    private Location location;

    public LocationEntity()
    {
    }

    public LocationEntity(String lat, String lng, String alt)
    {
        this.lat = lat;
        this.lng = lng;
        this.alt = alt;
    }

    public String getLat()
    {
        return lat;
    }

    public void setLat(String lat)
    {
        this.lat = lat;
    }

    public String getLng()
    {
        return lng;
    }

    public void setLng(String lng)
    {
        this.lng = lng;
    }

    public String getAlt()
    {
        return alt;
    }

    public void setAlt(String alt)
    {
        this.alt = alt;
    }

    public Location getLocation()
    {
        return location;
    }

    public void setLocation(Location location)
    {
        this.location = location;
    }

    public LatLng getLatLng()
    {
        return new LatLng(location.getLatitude(), location.getLongitude());
    }
}
