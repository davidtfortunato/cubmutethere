package isec.cub.mutethere.base.data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import isec.cub.mutethere.CUBApplication;

/**
 * Created by David Fortunato on 27/05/2017
 */

public class PreferencesHelper
{

    // Shared Name
    private static final String SHARED_PREFERENCES_NAME = PreferencesHelper.class.getName() + "SharedPreferences";

    /**
     * Put a new String value into a key index
     * @param key
     * @param value
     */
    public static void putStringMessage(String key, String value)
    {
        SharedPreferences sharedPreferences = CUBApplication.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(key, value).commit();
    }

    /**
     * Get a String from preferences
     * @param key
     */
    public static String getStringMessage(String key, String defaultValue)
    {
        SharedPreferences sharedPreferences = CUBApplication.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, defaultValue);
    }

    public static void putIntValue(String key, int value)
    {
        SharedPreferences sharedPreferences = CUBApplication.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putInt(key, value).commit();
    }

    public static int getIntValue(String key, int defaultValue)
    {
        SharedPreferences sharedPreferences = CUBApplication.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(key, defaultValue);
    }

    public static void putLongValue(String key, long value)
    {
        SharedPreferences sharedPreferences = CUBApplication.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putLong(key, value).commit();
    }

    public static long getLongValue(String key, long defaultValue)
    {
        SharedPreferences sharedPreferences = CUBApplication.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getLong(key, defaultValue);
    }

    public static void putObjectData(String key, Object object)
    {
        String jsonObject = "";
        if (object != null)
        {
            jsonObject = GSONParser.parseObjectToString(object);
        }

        putStringMessage(key, jsonObject);
    }

    public static <Type> Type getObjectData(String key, Class<Type> objectClass)
    {
        String jsonObject = getStringMessage(key, null);
        if (jsonObject != null)
        {
            return GSONParser.parseJSONToObject(jsonObject, objectClass);
        }
        else
        {
            return null;
        }
    }

}
