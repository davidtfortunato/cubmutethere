package isec.cub.mutethere.base.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by David Fortunato on 23/07/2017
 * All rights reserved GoodBarber
 */

public class AlertLocationReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        AlertLocationService.startService(context);
    }
}
