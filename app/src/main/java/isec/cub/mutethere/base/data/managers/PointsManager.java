package isec.cub.mutethere.base.data.managers;

import java.util.ArrayList;
import java.util.List;
import isec.cub.mutethere.CUBApplication;
import isec.cub.mutethere.Utils;
import isec.cub.mutethere.base.data.PointAlertEntity;
import isec.cub.mutethere.base.data.PointMemoAlertEntity;
import isec.cub.mutethere.base.data.PointSoundAlertEntity;

/**
 * Created by David Fortunato on 19/07/2017
 * All rights reserved GoodBarber
 */

public class PointsManager
{

    private static PointsManager instance;

    // Constants
    private static final String PREFS_KEY_SOUND_LIST_ALERTS = PointsManager.class.getName() + ".PREFS_KEY_SOUND_LIST_ALERTS";
    private static final String PREFS_KEY_MEMO_LIST_ALERTS = PointsManager.class.getName() + ".PREFS_KEY_MEMO_LIST_ALERTS";

    // Data
    private List<PointAlertEntity> listAlerts;

    private PointsManager()
    {
        loadData();
    }

    public static PointsManager getInstance()
    {
        if (instance == null)
        {
            instance = new PointsManager();
        }
        return instance;
    }

    public List<PointAlertEntity> getListAlerts()
    {
        return listAlerts;
    }

    private void loadData()
    {
        // Clean list alers
        if (listAlerts == null)
        {
            listAlerts = new ArrayList<>();
        }
        else
        {
            listAlerts.clear();
        }

        // Load Sounds
        String jsonSoundData = PreferencesHelper.getStringMessage(PREFS_KEY_SOUND_LIST_ALERTS, null);
        if (jsonSoundData != null)
        {
            listAlerts.addAll(GSONParser.parseJSONToList(jsonSoundData, PointSoundAlertEntity.class));
        }

        // Load Memos
        String jsonMemoData = PreferencesHelper.getStringMessage(PREFS_KEY_MEMO_LIST_ALERTS, null);
        if (jsonMemoData != null)
        {
            listAlerts.addAll(GSONParser.parseJSONToList(jsonMemoData, PointMemoAlertEntity.class));
        }
    }

    public void saveData()
    {
        cleanDummyData();
        String jsonData = GSONParser.parseListObjectToString(getListFilteredPoints(PointAlertEntity.PointType.MEMO));
        PreferencesHelper.putStringMessage(PREFS_KEY_MEMO_LIST_ALERTS, jsonData);
        jsonData = GSONParser.parseListObjectToString(getListFilteredPoints(PointAlertEntity.PointType.SOUND));
        PreferencesHelper.putStringMessage(PREFS_KEY_SOUND_LIST_ALERTS, jsonData);
    }

    public void addPointAlert(PointAlertEntity pointAlertEntity)
    {
        if (existsPointId(pointAlertEntity.getId()))
        {
            // Replace old Point Alert
            listAlerts.remove(getPointById(pointAlertEntity.getId()));
        }
        listAlerts.add(pointAlertEntity);
        saveData();
    }


    public void removePointAlert(String id)
    {
        PointAlertEntity point = getPointById(id);
        if (point != null)
        {
            listAlerts.remove(point);
            saveData();
        }
    }

    public PointAlertEntity getPointById(String id)
    {
        for (PointAlertEntity point : listAlerts)
        {
            if (point.getId().equalsIgnoreCase(id))
            {
                return point;
            }
        }
        return null;
    }

    public boolean existsPointId(String id)
    {
        PointAlertEntity point = getPointById(id);
        return point != null;
    }

    public void cleanDummyData()
    {
        for (PointAlertEntity pointAlertEntity : listAlerts)
        {
            pointAlertEntity.cleanDummyData();
        }
    }

    public List<PointAlertEntity> getListFilteredPoints(PointAlertEntity.PointType pointType)
    {
        List<PointAlertEntity> listFiltered = new ArrayList<>();

        for (PointAlertEntity point : getListAlerts())
        {
            if (point.getPointType() == pointType)
            {
                listFiltered.add(point);
            }
        }
        return listFiltered;
    }

    public void executeSoundAlert(PointSoundAlertEntity soundAlertEntity)
    {
        // Execute Sound Alert Entity
        Utils.changeSound(CUBApplication.getContext(), soundAlertEntity.getSoundAlertMode());
    }

    public void executeMemoAlert(PointMemoAlertEntity pointMemoAlertEntity)
    {
        // Execute Sound Alert Entity
        Utils.displayMemo(CUBApplication.getContext(),pointMemoAlertEntity);

        // Check if should remove it
        if (pointMemoAlertEntity.getMemoRepeatMode() == PointMemoAlertEntity.MemoRepeatMode.ONE_SHOT)
        {
            // Remove alert
            removePointAlert(pointMemoAlertEntity.getId());
        }
    }

}
