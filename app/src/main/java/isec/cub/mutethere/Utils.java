package isec.cub.mutethere;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.support.v4.app.NotificationCompat;
import com.google.android.gms.maps.model.LatLng;
import isec.cub.mutethere.base.data.PointMemoAlertEntity;
import isec.cub.mutethere.base.data.PointSoundAlertEntity;
import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by David Fortunato on 22/07/2017
 * All rights reserved GoodBarber
 */

public class Utils
{

    public static Location convertLatLngToLocation(LatLng latLng)
    {
        Location temp = new Location(LocationManager.GPS_PROVIDER);
        temp.setLatitude(latLng.latitude);
        temp.setLongitude(latLng.longitude);
        return temp;
    }


    public static void changeSound(Context context, PointSoundAlertEntity.SoundAlertMode soundAlertMode)
    {
        AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        switch (soundAlertMode)
        {
            case ACTIVATE_SOUND:
                audio.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                break;
            case MUTE_SOUND:
                audio.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                break;
        }
    }

    public static void displayMemo(Context context, PointMemoAlertEntity memoAlertEntity)
    {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.cast_ic_notification_small_icon)
                        .setContentTitle("Memo Alert")
                        .setContentText(memoAlertEntity.getName());

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, MainActivity.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        // Sets an ID for the notification
        int mNotificationId = 001;
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    public static int[] parseTimeStringToTimeArray(String time)
    {
        String[] timeDate = time.split(":");
        int[] timeDateInt = new int[2];
        timeDateInt[0] = Integer.valueOf(timeDate[0]);
        timeDateInt[1] = Integer.valueOf(timeDate[1]);

        return timeDateInt;
    }

    public static boolean isArrTimeAfter(int []startTimeArr, int []currentTimeArr)
    {
        return startTimeArr[0] <= currentTimeArr[0] && startTimeArr[1] <= currentTimeArr[1];
    }

    public static boolean isArrTimeBefore(int []endTimeArr, int []currentTimeArr)
    {
        return endTimeArr[0] >= currentTimeArr[0] && endTimeArr[1] >= currentTimeArr[1];
    }
}
