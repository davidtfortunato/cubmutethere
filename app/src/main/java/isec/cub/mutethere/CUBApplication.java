package isec.cub.mutethere;

import android.app.Application;
import android.content.Context;

/**
 * Created by David Fortunato on 27/05/2017
 */

public class CUBApplication extends Application
{
    private static Application mApplicationInstance;

    @Override
    public void onCreate()
    {
        super.onCreate();
        mApplicationInstance = this;
    }

    public static Application getApplicationInstance()
    {
        return mApplicationInstance;
    }

    public static Context getContext()
    {
        return getApplicationInstance().getBaseContext();
    }
}
