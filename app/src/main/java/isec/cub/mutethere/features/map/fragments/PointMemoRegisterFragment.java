package isec.cub.mutethere.features.map.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RadioButton;
import com.google.android.gms.maps.model.CircleOptions;
import isec.cub.mutethere.R;
import isec.cub.mutethere.Utils;
import isec.cub.mutethere.base.data.PointMemoAlertEntity;

/**
 * Created by David Fortunato on 22/07/2017
 * All rights reserved GoodBarber
 */

public class PointMemoRegisterFragment extends PointAlertRegisterBaseFragment
{

    // Radio Buttons
    private RadioButton mRbtnModeAlwaysRepeat;
    private RadioButton mRbtnModeOneShot;

    public static Fragment newInstance(CircleOptions circleOptions)
    {
        PointMemoRegisterFragment fragment = new PointMemoRegisterFragment();
        PointMemoAlertEntity entity = new PointMemoAlertEntity();
        entity.setRadius(circleOptions.getRadius());
        entity.setCenterLocation(Utils.convertLatLngToLocation(circleOptions.getCenter()));

        Bundle args = new Bundle();
        args.putSerializable(PointSoundAlertRegisterBaseFragment.EXTRA_INIT_POINT_OBJ, entity);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    protected void initUI(ViewGroup layoutContent)
    {
        super.initUI(layoutContent);

        // Inflate Sound Form Layout
        LayoutInflater.from(getContext()).inflate(R.layout.fragment_register_memo_alert, getContainerForm(), true);

        // find views
        mRbtnModeAlwaysRepeat = (RadioButton) layoutContent.findViewById(R.id.rtbn_mode_always_repeat);
        mRbtnModeOneShot = (RadioButton) layoutContent.findViewById(R.id.rtbn_mode_one_shot);

        mRbtnModeAlwaysRepeat.setChecked(true);
    }

    @Override
    protected void onRefreshEntityData()
    {
        super.onRefreshEntityData();
        ((PointMemoAlertEntity) getPointAlertEntity()).setMemoRepeatMode(mRbtnModeAlwaysRepeat.isChecked() ? PointMemoAlertEntity.MemoRepeatMode.EVERY_TIME_INSIDE : PointMemoAlertEntity.MemoRepeatMode.ONE_SHOT);
    }

}
