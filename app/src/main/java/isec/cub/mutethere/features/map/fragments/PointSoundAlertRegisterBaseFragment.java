package isec.cub.mutethere.features.map.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RadioButton;
import com.google.android.gms.maps.model.CircleOptions;
import isec.cub.mutethere.R;
import isec.cub.mutethere.Utils;
import isec.cub.mutethere.base.data.PointSoundAlertEntity;

/**
 * Created by David Fortunato on 22/07/2017
 * All rights reserved GoodBarber
 */

public class PointSoundAlertRegisterBaseFragment extends PointAlertRegisterBaseFragment
{

    // Views
    private RadioButton mRBTNActivateSound;
    private RadioButton mRBTNDeactivateSound;

    public static Fragment newInstance(CircleOptions circleOptions)
    {
        PointSoundAlertRegisterBaseFragment fragment = new PointSoundAlertRegisterBaseFragment();
        PointSoundAlertEntity entity = new PointSoundAlertEntity();
        entity.setRadius(circleOptions.getRadius());
        entity.setCenterLocation(Utils.convertLatLngToLocation(circleOptions.getCenter()));

        Bundle args = new Bundle();
        args.putSerializable(PointSoundAlertRegisterBaseFragment.EXTRA_INIT_POINT_OBJ, entity);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    protected void initUI(ViewGroup layoutContent)
    {
        super.initUI(layoutContent);

        // Inflate Sound Form Layout
        LayoutInflater.from(getContext()).inflate(R.layout.fragment_register_sound_alert, getContainerForm(), true);

        // find views
        mRBTNActivateSound = (RadioButton) layoutContent.findViewById(R.id.rtbn_mode_activate_sound);
        mRBTNDeactivateSound = (RadioButton) layoutContent.findViewById(R.id.rtbn_mode_deactivate_sound);

        mRBTNActivateSound.setChecked(true);
    }

    @Override
    protected void onRefreshEntityData()
    {
        super.onRefreshEntityData();
        ((PointSoundAlertEntity) getPointAlertEntity()).setSoundAlertMode(mRBTNActivateSound.isChecked() ? PointSoundAlertEntity.SoundAlertMode.ACTIVATE_SOUND : PointSoundAlertEntity.SoundAlertMode.MUTE_SOUND);
    }
}
