package isec.cub.mutethere.features.list.indicators;

import com.goodbarber.recyclerindicator.BaseUIParameters;
import com.goodbarber.recyclerindicator.GBBaseRecyclerAdapter;
import com.goodbarber.recyclerindicator.GBRecyclerViewHolder;
import isec.cub.mutethere.base.data.PointMemoAlertEntity;
import isec.cub.mutethere.features.list.views.AlertListItemView;

/**
 * Created by David Fortunato on 23/07/2017
 * All rights reserved GoodBarber
 */

public class PointAlertMemoIndicator extends PointAlertBaseIndicator<PointMemoAlertEntity>
{
    public PointAlertMemoIndicator(PointMemoAlertEntity dataObject)
    {
        super(dataObject);
    }

    @Override
    public void refreshCell(GBRecyclerViewHolder<AlertListItemView> gbRecyclerViewHolder, GBBaseRecyclerAdapter gbBaseRecyclerAdapter,
                            BaseUIParameters baseUIParameters, int i, int i1)
    {
        super.refreshCell(gbRecyclerViewHolder, gbBaseRecyclerAdapter, baseUIParameters, i, i1);

        gbRecyclerViewHolder.getView().getTVAlertMode().setText("Modo: " + getObjectData().getMemoRepeatMode().name());
    }
}
