package isec.cub.mutethere.features.map.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import isec.cub.mutethere.R;
import isec.cub.mutethere.base.data.PointAlertEntity;
import isec.cub.mutethere.base.data.managers.PointsManager;
import isec.cub.mutethere.base.fragments.BaseFragment;
import isec.cub.mutethere.features.list.fragments.ListAlertsFragment;
import isec.cub.mutethere.features.views.ButtonTimePick;

/**
 * Created by David Fortunato on 22/07/2017
 * All rights reserved GoodBarber
 */

public class PointAlertRegisterBaseFragment extends BaseFragment
{
    // Extras
    public static final String EXTRA_INIT_POINT_OBJ = "pointObject";

    //  Views
    private LinearLayout         mContainerForm;
    private EditText             mETName;
    private CheckBox             mCBIsInside;
    private ButtonTimePick       mBtnStartTime;
    private ButtonTimePick       mBtnEndTime;
    private FloatingActionButton mFABDone;

    // Data
    private PointAlertEntity mPointAlertEntity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mPointAlertEntity = (PointAlertEntity) getArguments().getSerializable(EXTRA_INIT_POINT_OBJ);
    }

    @Override
    protected int getLayoutId()
    {
        return R.layout.fragment_register_point;
    }

    @Override
    protected void initUI(ViewGroup layoutContent)
    {
        // find views
        mContainerForm = (LinearLayout) layoutContent.findViewById(R.id.container_form);
        mETName = (EditText) layoutContent.findViewById(R.id.et_name);
        mCBIsInside = (CheckBox) layoutContent.findViewById(R.id.checkbox_inside);
        mBtnStartTime = (ButtonTimePick) layoutContent.findViewById(R.id.btn_start_time);
        mBtnEndTime = (ButtonTimePick) layoutContent.findViewById(R.id.btn_end_time);
        mFABDone = (FloatingActionButton) layoutContent.findViewById(R.id.fab_plus);

        // Set background
        layoutContent.setBackgroundColor(Color.WHITE);

        // Set Listener
        mFABDone.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onRefreshEntityData();
                addEntity();
            }
        });
    }

    protected void addEntity()
    {
        // Check if is valid
        if (getPointAlertEntity().isValidData())
        {
            PointsManager.getInstance().addPointAlert(getPointAlertEntity());
            getActivity().getSupportFragmentManager().popBackStackImmediate(ListAlertsFragment.class.getName(), 0);
        }
    }

    protected void onRefreshEntityData()
    {
        mPointAlertEntity.setName(mETName.getText().toString());
        mPointAlertEntity.setInside(mCBIsInside.isChecked());
        mPointAlertEntity.setDailyHourStart(mBtnStartTime.getTimeString());
        mPointAlertEntity.setDailyHourEnd(mBtnEndTime.getTimeString());
    }

    public PointAlertEntity getPointAlertEntity()
    {
        return mPointAlertEntity;
    }

    private String getTime(EditText editText)
    {
        return editText.toString();
    }

    public LinearLayout getContainerForm()
    {
        return mContainerForm;
    }
}
