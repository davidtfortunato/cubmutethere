package isec.cub.mutethere.features.list.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import isec.cub.mutethere.R;

/**
 * Created by David Fortunato on 23/07/2017
 * All rights reserved GoodBarber
 */

public class AlertListItemView extends RelativeLayout
{

    // Views
    private TextView     mTVAlertType;
    private TextView     mTVAlertTime;
    private TextView     mTVAlertTitle;
    private TextView     mTVAlertMode;
    private LinearLayout mExtraContent;

    public AlertListItemView(Context context)
    {
        super(context);
        initUI();
    }

    public AlertListItemView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initUI();
    }

    public AlertListItemView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        initUI();
    }

    protected void initUI()
    {
        LayoutInflater.from(getContext()).inflate(R.layout.indicator_alert_register, this, true);

        mTVAlertType = (TextView) findViewById(R.id.tv_alert_type);
        mTVAlertTime = (TextView) findViewById(R.id.tv_alert_time);
        mTVAlertTitle = (TextView) findViewById(R.id.tv_alert_title);
        mTVAlertMode = (TextView) findViewById(R.id.tv_alert_mode);
        mExtraContent = (LinearLayout) findViewById(R.id.layout_extra_content);
    }

    public TextView getTVAlertType()
    {
        return mTVAlertType;
    }

    public TextView getTVAlertTime()
    {
        return mTVAlertTime;
    }

    public TextView getTVAlertTitle()
    {
        return mTVAlertTitle;
    }

    public TextView getTVAlertMode()
    {
        return mTVAlertMode;
    }

    public LinearLayout getExtraContent()
    {
        return mExtraContent;
    }
}
