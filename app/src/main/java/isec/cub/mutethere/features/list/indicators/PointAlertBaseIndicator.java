package isec.cub.mutethere.features.list.indicators;

import android.content.Context;
import android.view.ViewGroup;
import com.goodbarber.recyclerindicator.BaseUIParameters;
import com.goodbarber.recyclerindicator.GBBaseRecyclerAdapter;
import com.goodbarber.recyclerindicator.GBRecyclerViewHolder;
import com.goodbarber.recyclerindicator.GBRecyclerViewIndicator;
import isec.cub.mutethere.base.data.PointAlertEntity;
import isec.cub.mutethere.features.list.views.AlertListItemView;

/**
 * Created by David Fortunato on 23/07/2017
 * All rights reserved GoodBarber
 */

public class PointAlertBaseIndicator<T extends PointAlertEntity> extends GBRecyclerViewIndicator<AlertListItemView, T, BaseUIParameters>
{

    public PointAlertBaseIndicator(T dataObject)
    {
        super(dataObject);
    }

    @Override
    public AlertListItemView getViewCell(Context context, ViewGroup viewGroup)
    {
        return new AlertListItemView(context);
    }

    @Override
    public void initCell(GBRecyclerViewHolder<AlertListItemView> gbRecyclerViewHolder, BaseUIParameters baseUIParameters)
    {
        gbRecyclerViewHolder.getView().setBackgroundColor(getObjectData().getColor());
    }

    @Override
    public void refreshCell(GBRecyclerViewHolder<AlertListItemView> gbRecyclerViewHolder, GBBaseRecyclerAdapter gbBaseRecyclerAdapter,
                            BaseUIParameters baseUIParameters, int i, int i1)
    {
        gbRecyclerViewHolder.getView().getTVAlertType().setText("Tipo: " + getObjectData().getPointType().name());
        gbRecyclerViewHolder.getView().getTVAlertTime().setText("Hora de Inicio: " + getObjectData().getDailyHourStart() + " - " + " Hora final: " + getObjectData().getDailyHourEnd());
        gbRecyclerViewHolder.getView().getTVAlertTitle().setText(getObjectData().getName());
    }

    @Override
    public BaseUIParameters getUIParameters(String s)
    {
        return new BaseUIParameters();
    }
}
