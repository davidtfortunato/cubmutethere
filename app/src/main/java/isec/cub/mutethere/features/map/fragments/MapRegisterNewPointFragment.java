package isec.cub.mutethere.features.map.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import isec.cub.mutethere.R;
import isec.cub.mutethere.base.controllers.CUBLocationManager;
import isec.cub.mutethere.base.data.LocationEntity;
import isec.cub.mutethere.base.data.PointAlertEntity;
import isec.cub.mutethere.base.data.PointMemoAlertEntity;
import isec.cub.mutethere.base.data.PointSoundAlertEntity;
import isec.cub.mutethere.base.fragments.BaseFragment;

/**
 * Created by David Fortunato on 27/06/2017
 * All rights reserved GoodBarber
 */

public class MapRegisterNewPointFragment extends BaseFragment implements OnMapReadyCallback, CUBLocationManager.OnReportLocation, View.OnClickListener, GoogleMap.OnCameraMoveListener
{

    // Extras
    private static final String EXTRA_POINT_TYPE = "pointType";

    // Default values
    private static final float DEFAULT_ZOOM = 16;
    private static final int DEFAULT_RADIUS = 200;
    private static final int RADIUS_INCREASE_STEP = 10;


    // Views
    private MapView              mMapView;
    private FloatingActionButton mFabPlus;
    private FloatingActionButton mFabMinus;
    private FloatingActionButton mFabDone;

    // Google Map
    private GoogleMap mGoogleMap;

    // Location Manager
    private CUBLocationManager mCubLocationManager;

    // Data
    private PointAlertEntity.PointType mPointType;
    private PointAlertEntity           mPointCreated;
    private CircleOptions              circleOptions;

    public static MapRegisterNewPointFragment newInstance(PointAlertEntity.PointType pointType)
    {
        MapRegisterNewPointFragment fragment = new MapRegisterNewPointFragment();

        // Set Arguments
        Bundle extras = new Bundle();
        extras.putSerializable(EXTRA_POINT_TYPE, pointType);
        fragment.setArguments(extras);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Extras
        mPointType = (PointAlertEntity.PointType) getArguments().getSerializable(EXTRA_POINT_TYPE);

        // data
        circleOptions = new CircleOptions();
        circleOptions.strokeWidth(0);

        switch (mPointType)
        {
            case SOUND:
                mPointCreated = new PointSoundAlertEntity();
                circleOptions.fillColor(Color.parseColor(PointAlertEntity.SOUND_COLOR));
                break;
            case MEMO:
                mPointCreated = new PointMemoAlertEntity();
                circleOptions.fillColor(Color.parseColor(PointAlertEntity.MEMO_COLOR));
                break;
        }

        mCubLocationManager = new CUBLocationManager(getActivity(), this);
        mCubLocationManager.initLocation();
    }

    @Override
    protected int getLayoutId()
    {
        return R.layout.fragment_register_map_point;
    }

    @Override
    protected void initUI(ViewGroup layoutContent)
    {
        mMapView = (MapView) layoutContent.findViewById(R.id.mapView);
        mFabPlus = (FloatingActionButton) layoutContent.findViewById(R.id.fab_plus);
        mFabMinus = (FloatingActionButton) layoutContent.findViewById(R.id.fab_minus);
        mFabDone = (FloatingActionButton) layoutContent.findViewById(R.id.fab_done);

        // Initialize Map View
        mMapView.getMapAsync(this);

        // Set listeners
        mFabPlus.setOnClickListener(this);
        mFabMinus.setOnClickListener(this);
        mFabDone.setOnClickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View v = super.onCreateView(inflater, container, savedInstanceState);

        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        // Setup Google Maps
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        // Request permission
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            mGoogleMap.setMyLocationEnabled(true);
            circleOptions.center(mCubLocationManager.getLastLocationEntity().getLatLng());
        }

        CameraPosition cameraPosition = CameraPosition.fromLatLngZoom(mCubLocationManager.getLastLocationEntity().getLatLng(), DEFAULT_ZOOM);
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        mGoogleMap.setOnCameraMoveListener(this);
        // Circle
        circleOptions.radius(DEFAULT_RADIUS);
        printCircle();
    }

    private void printCircle()
    {
        mGoogleMap.clear();
        mGoogleMap.addCircle(circleOptions);
    }

    @Override
    public void onReportLocation(LocationEntity locationEntity)
    {
        if (mGoogleMap != null)
        {
            mGoogleMap.getProjection().toScreenLocation(new LatLng(locationEntity.getLocation().getLatitude(), locationEntity.getLocation().getLongitude()));
        }
    }

    @Override
    public void onClick(View v)
    {
        if (v == mFabPlus)
        {
            // Increase Radius
            circleOptions.radius(circleOptions.getRadius() + RADIUS_INCREASE_STEP);
            printCircle();
        }
        else if (v == mFabMinus)
        {
            // Decrease Radius
            circleOptions.radius(circleOptions.getRadius() - RADIUS_INCREASE_STEP);
            printCircle();
        }
        else if (v == mFabDone)
        {
            // Open Form
            switch (mPointType)
            {
                case SOUND:
                    navigateToFragment(PointSoundAlertRegisterBaseFragment.newInstance(circleOptions));
                    break;
                case MEMO:
                    navigateToFragment(PointMemoRegisterFragment.newInstance(circleOptions));
                    break;
            }

        }
    }

    @Override
    public void onCameraMove()
    {
        if (mGoogleMap !=null)
        {
            circleOptions.center(mGoogleMap.getProjection().getVisibleRegion().latLngBounds.getCenter());
            printCircle();
        }
    }
}
