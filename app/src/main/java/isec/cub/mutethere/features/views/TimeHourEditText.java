package isec.cub.mutethere.features.views;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;

/**
 * Created by David Fortunato on 23/07/2017
 * All rights reserved GoodBarber
 */

public class TimeHourEditText extends android.support.v7.widget.AppCompatEditText
{
    private static final String TAG = TimeHourEditText.class.getSimpleName();

    public TimeHourEditText(Context context)
    {
        super(context);
    }

    public TimeHourEditText(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public TimeHourEditText(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    private void initUI()
    {
        addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });
    }

    @Override
    public void setText(CharSequence text, BufferType type)
    {
        try
        {
            int value = Integer.parseInt(text.toString());
            if (value >= 0 && value < 23)
            {
                super.setText(text, type);
            }
        } catch (NumberFormatException e)
        {
            Log.e(TAG, e.getMessage(), e);
            super.setText("0", type);
        }


    }
}
