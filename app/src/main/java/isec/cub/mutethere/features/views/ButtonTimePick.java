package isec.cub.mutethere.features.views;

import android.app.TimePickerDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TimePicker;

/**
 * Created by David Fortunato on 23/07/2017
 * All rights reserved GoodBarber
 */

public class ButtonTimePick extends android.support.v7.widget.AppCompatButton implements TimePickerDialog.OnTimeSetListener
{

    private int hourDay;
    private int minuteDay;

    public ButtonTimePick(Context context)
    {
        super(context);
        initUI();
    }

    public ButtonTimePick(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initUI();
    }

    public ButtonTimePick(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        initUI();
    }

    private void initUI()
    {
        hourDay = 0;
        minuteDay = 0;
        setText(getTimeString());
        setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                TimePickerDialog timePicker = new TimePickerDialog(getContext(), ButtonTimePick.this, hourDay, minuteDay, true);
                timePicker.show();
            }
        });
    }

    public String getTimeString()
    {
        return hourDay + ":" + minuteDay;
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute)
    {
        this.hourDay = hourOfDay;
        this.minuteDay = minute;
        setText(getTimeString());
    }
}
