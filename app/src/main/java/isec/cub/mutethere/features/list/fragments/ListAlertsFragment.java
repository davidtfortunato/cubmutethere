package isec.cub.mutethere.features.list.fragments;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.goodbarber.recyclerindicator.GBBaseRecyclerAdapter;
import com.goodbarber.recyclerindicator.GBRecyclerView;
import com.goodbarber.recyclerindicator.GBRecyclerViewIndicator;
import isec.cub.mutethere.R;
import isec.cub.mutethere.base.data.PointAlertEntity;
import isec.cub.mutethere.base.data.managers.PointsManager;
import isec.cub.mutethere.base.fragments.BaseFragment;
import isec.cub.mutethere.features.list.adapters.ListAlertsAdapter;
import isec.cub.mutethere.features.map.fragments.MapDetailsPointFragment;

/**
 * Created by David Fortunato on 23/07/2017
 * All rights reserved GoodBarber
 */

public class ListAlertsFragment extends BaseFragment implements GBBaseRecyclerAdapter.OnClickRecyclerAdapterViewListener
{

    // Recycler View
    private GBRecyclerView recyclerView;
    private TextView mTVEmptyMessage;

    // Adapter
    private ListAlertsAdapter listAdaper;

    @Override
    protected int getLayoutId()
    {
        return R.layout.fragment_list_layout;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        loadData();
    }

    @Override
    protected void initUI(ViewGroup layoutContent)
    {
        recyclerView = (GBRecyclerView) layoutContent.findViewById(R.id.recycler_view);
        mTVEmptyMessage = (TextView) layoutContent.findViewById(R.id.tv_empty_message);

        // Init adapter
        listAdaper = new ListAlertsAdapter(getActivity());
        recyclerView.setGBAdapter(listAdaper);
        listAdaper.setOnClickRecyclerAdapterViewListener(this);

        loadData();
    }

    private void loadData()
    {
        if (listAdaper != null)
        {
            listAdaper.addListData(PointsManager.getInstance().getListAlerts(), true);

            // Check if should display empty message
            mTVEmptyMessage.setVisibility(PointsManager.getInstance().getListAlerts().size() > 0 ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onItemClick(View view, GBRecyclerViewIndicator gbRecyclerViewIndicator, int i)
    {
        navigateToFragment(MapDetailsPointFragment.newInstance((PointAlertEntity) gbRecyclerViewIndicator.getObjectData()));
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        getActivity().finish();
    }
}
