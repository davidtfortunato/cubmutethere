package isec.cub.mutethere.features.list.adapters;

import android.content.Context;
import com.goodbarber.recyclerindicator.GBBaseAdapterConfigs;
import com.goodbarber.recyclerindicator.GBBaseRecyclerAdapter;
import com.goodbarber.recyclerindicator.GBRecyclerViewIndicator;
import java.util.ArrayList;
import java.util.List;
import isec.cub.mutethere.base.data.PointAlertEntity;
import isec.cub.mutethere.base.data.PointMemoAlertEntity;
import isec.cub.mutethere.base.data.PointSoundAlertEntity;
import isec.cub.mutethere.features.list.indicators.PointAlertMemoIndicator;
import isec.cub.mutethere.features.list.indicators.PointAlertSoundIndicator;

/**
 * Created by David Fortunato on 23/07/2017
 * All rights reserved GoodBarber
 */

public class ListAlertsAdapter extends GBBaseRecyclerAdapter<PointAlertEntity>
{
    public ListAlertsAdapter(Context activity)
    {
        super(activity, new GBBaseAdapterConfigs.Builder().setLayoutManagerOrientation(GBBaseAdapterConfigs.VERTICAL).build());
    }

    @Override
    public void addListData(List<PointAlertEntity> list, boolean b)
    {
        List<GBRecyclerViewIndicator> listIndicators = new ArrayList<>();

        for (PointAlertEntity alertEntity : list)
        {
            if (alertEntity instanceof PointSoundAlertEntity)
            {
                listIndicators.add(new PointAlertSoundIndicator((PointSoundAlertEntity) alertEntity));
            }
            else if (alertEntity instanceof PointMemoAlertEntity)
            {
                listIndicators.add(new PointAlertMemoIndicator((PointMemoAlertEntity) alertEntity));
            }

        }

        addGBListIndicators(listIndicators, b);
    }

    @Override
    public int getGBColumnsCount()
    {
        return 1;
    }

    @Override
    public GBRecyclerLayoutManagerType getLayoutManagerType()
    {
        return GBRecyclerLayoutManagerType.LINEAR_LAYOUT;
    }
}
