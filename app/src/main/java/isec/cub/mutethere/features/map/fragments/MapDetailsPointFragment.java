package isec.cub.mutethere.features.map.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import isec.cub.mutethere.R;
import isec.cub.mutethere.base.controllers.CUBLocationManager;
import isec.cub.mutethere.base.data.LocationEntity;
import isec.cub.mutethere.base.data.PointAlertEntity;
import isec.cub.mutethere.base.data.managers.PointsManager;
import isec.cub.mutethere.base.fragments.BaseFragment;
import isec.cub.mutethere.features.list.fragments.ListAlertsFragment;

/**
 * Created by David Fortunato on 27/06/2017
 * All rights reserved GoodBarber
 */

public class MapDetailsPointFragment extends BaseFragment implements OnMapReadyCallback, CUBLocationManager.OnReportLocation, View.OnClickListener
{

    // Extras
    private static final String EXTRA_POINT_DETAIL = "pointDetail";

    // Default values
    private static final float DEFAULT_ZOOM = 16;

    private MapView mMapView;
    private FloatingActionButton mFABMinus;

    // Google Map
    private GoogleMap mGoogleMap;

    // Location Manager
    private CUBLocationManager mCubLocationManager;

    // Data
    private PointAlertEntity alertEntity;

    public static MapDetailsPointFragment newInstance(PointAlertEntity pointAlertEntity)
    {
        MapDetailsPointFragment fragment = new MapDetailsPointFragment();

        // Set Arguments
        Bundle extras = new Bundle();
        extras.putSerializable(EXTRA_POINT_DETAIL, pointAlertEntity);
        fragment.setArguments(extras);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Extras
        alertEntity = (PointAlertEntity) getArguments().getSerializable(EXTRA_POINT_DETAIL);

        mCubLocationManager = new CUBLocationManager(getActivity(), this);
        mCubLocationManager.initLocation();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        printCircle(alertEntity);
    }

    @Override
    protected int getLayoutId()
    {
        return R.layout.fragment_map_point;
    }

    @Override
    protected void initUI(ViewGroup layoutContent)
    {
        mMapView = (MapView) layoutContent.findViewById(R.id.mapView);
        mFABMinus = (FloatingActionButton) layoutContent.findViewById(R.id.fab_plus);
        mFABMinus.setImageResource(R.drawable.ic_minus);
        mFABMinus.setOnClickListener(this);

        // Initialize Map View
        mMapView.getMapAsync(this);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View v = super.onCreateView(inflater, container, savedInstanceState);

        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        // Setup Google Maps
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        // Request permission
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            mGoogleMap.setMyLocationEnabled(true);
        }

        // Print Points
        printCircle(alertEntity);

        CameraPosition cameraPosition = CameraPosition.fromLatLngZoom(alertEntity.getLatLng(), DEFAULT_ZOOM);
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }

    /**
     * Print all circle points
     */
    private void printCircle(PointAlertEntity pointAlertEntity)
    {
        if (mGoogleMap != null)
        {
            // Clean all circles
            mGoogleMap.clear();

            mGoogleMap.addCircle(pointAlertEntity.getCircleOptions());
        }
    }

    @Override
    public void onReportLocation(LocationEntity locationEntity)
    {
        if (mGoogleMap != null)
        {
            mGoogleMap.getProjection().toScreenLocation(new LatLng(locationEntity.getLocation().getLatitude(), locationEntity.getLocation().getLongitude()));
        }
    }

    @Override
    public void onClick(View v)
    {
        if (v == mFABMinus)
        {
            PointsManager.getInstance().removePointAlert(alertEntity.getId());

            getActivity().getSupportFragmentManager().popBackStackImmediate(ListAlertsFragment.class.getName(), 0);
        }
    }
}
